import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {

  setTimeout(() => {
    document.title = "One new message";
  }, 3000);

  const button = document.querySelector(".button");
  button.addEventListener("click", () => {
    alert("💣");
  });
});
